import requests

url = "https://test.cashfree.com/api/v1/order/create"

payload = {
    "appId":"2724426defd8bd1d4b74258104244272",
    "secretKey":"436540a17f52b97f146e23ad049fd89bbd1fd280",
    "orderId":"order_2724422I0ABvOGAzeIj81dYPQv0chs0Pe5u",  # unique order id everytime
    "orderAmount":"1000",
    "orderCurrency":"INR",
    "orderNote":"NOthing optional",
    "customerName":"Jatin",
    "customerEmail":"jatin.mobcoder@gmail.com",
    "customerPhone":"7011355357",
    "returnUrl":"https://www.cashfree.com/",
    # "notifyUrl":"",
}

a = requests.request("POST",url,data=payload)   
print(a.text)