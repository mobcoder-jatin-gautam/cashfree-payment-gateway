from django.urls import path
from .views import *

urlpatterns = [
    path('v1/user/get-all-products/',GetAllProducts.as_view()),
    path('v1/user/make-payment/',PaymentView.as_view()),
    path('v1/user/order-pay/',OrderPay.as_view()),
]