from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(AccountModel)
class AccountAdmin(admin.ModelAdmin):
    list_display = ['_id', 'product_ids', 'price', 'payment_status']

@admin.register(ProductModel)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['_id', 'product_name', 'product_price']

