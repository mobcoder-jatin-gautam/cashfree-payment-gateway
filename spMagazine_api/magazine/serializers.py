from django.utils import timezone
from rest_framework import serializers
# from rest_meets_djongo.serializers import DjongoModelSerializer
from .models import (MagazineModel, AccountModel)
import json


class MagazineSerializer(serializers.ModelSerializer):
    """
    Serializer for uploading magazine.
    """
    created_date = serializers.ReadOnlyField()
    updated_date = serializers.ReadOnlyField()

    class Meta:
        model = MagazineModel
        fields = ['title', 'magazine_pic', 'magazine_url', 'description','created_date','updated_date','category','publisher_name','publisher_month']
        
    def create(self, validated_data):
        return MagazineModel.objects.create(**validated_data)

class PaymentSerializer(serializers.ModelSerializer):
    product_ids = serializers.JSONField(required=False)

    class Meta: 
        model = AccountModel
        fields = ['product_ids']
