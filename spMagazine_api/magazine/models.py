# from django.db import models
from djongo import models
import datetime


def magazine_directory_path(instance, filename):
    return f'magazines/{str(datetime.datetime.now()) + filename}'

class MagazineModel(models.Model):
    
    _id = models.ObjectIdField()
    title = models.CharField(max_length=120, default="")
    description = models.CharField(max_length=120, default="")
    category = models.CharField(max_length=120, default="")
    publisher_name = models.CharField(max_length=120, default="")
    publisher_month = models.CharField(max_length=120, default="")
    magazine_pic = models.ImageField(upload_to=magazine_directory_path, default="",null=True)
    magazine_url = models.FileField(upload_to=magazine_directory_path, default="",null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True)


class AccountModel(models.Model):
    _id = models.ObjectIdField()
    # user_id = models.CharField(max_length=120, default="")
    product_ids = models.JSONField(default=[])
    price = models.CharField(max_length=120, default="")
    payment_status = models.CharField(max_length=120, default="")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True)

class ProductModel(models.Model):
    _id = models.ObjectIdField()
    product_name = models.CharField(max_length=120, default="")
    product_price = models.CharField(max_length=120, default="")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True)