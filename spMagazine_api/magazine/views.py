import requests, json
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.response import Response
from .serializers import *
from rest_framework.parsers import FormParser, MultiPartParser
from .models import *
from bson.objectid import ObjectId


class GetAllProducts(generics.GenericAPIView):

    parser_classes = (FormParser, MultiPartParser)

    def get(self, request, *args, **kwargs):

        magazine_instance = ProductModel.objects.all().values('_id','product_name', 'product_price', 'updated_date')

        data = [{'_id':str(i['_id']),'product_name':i['product_name'], 'product_price':i['product_price'], 'updated_date':i['updated_date']}
                for i in magazine_instance]

        return Response({'status': 1,'responseData':{'data':data}},status=200)


class PaymentView(generics.GenericAPIView):

    parser_classes = (FormParser, MultiPartParser)
    serializer_class = PaymentSerializer

    def post(self, request, *args, **kwargs):

        # product_ids = request.data.get('product_ids')

        # print(product_ids)

        magazine_instance = ProductModel.objects.all().values('_id','product_name', 'product_price', 'updated_date')

        total = 0
        for i in magazine_instance:
            total = int(i['product_price']) + total

        print(total)


        url = "https://sandbox.cashfree.com/pg/orders"

        payload = {
            "customer_details": {
                "customer_id": "JAting",
                "customer_email": "jatin@gmail.com",
                "customer_phone": "9876543211"
            },
            "order_amount": total,
            "order_currency": "INR"
        }
        headers = {
            "accept": "application/json",
            "x-client-id": "2724426defd8bd1d4b74258104244272",
            "x-client-secret": "436540a17f52b97f146e23ad049fd89bbd1fd280", 
            "x-api-version": "2022-09-01",
            "content-type": "application/json"
        }
        # move client-id and client-secret in .env

        response = requests.post(url, json=payload, headers=headers)

        print(response.text)

        return Response({'status': 1,'responseData':{'message':json.loads(response.text)}},status=200)

class OrderPay(generics.GenericAPIView):


    parser_classes = (FormParser, MultiPartParser)
    serializer_class = PaymentSerializer

    def post(self, request, *args, **kwargs):
        
        url = "https://sandbox.cashfree.com/pg/orders/sessions"

        payload = {
            "payment_method": {"upi": {
                    "channel": "link",
                    "upi_id": "rajnandan1@okhdfcbak",
                    "upi_expiry_minutes": 10
                }},
            "payment_session_id": "session_bYT93pKoYv7hm1uVuT2GDR_Plwl9cKcfkfpM23MMmZdIQp_s6zpTH1sZOmSiSx8DLRCzKqO5Y5gooMF91RSvfjdWC1h12phKSC2iU5e_K_df"
        }
        headers = {
            "accept": "application/json",
            "x-api-version": "2022-09-01",
            "content-type": "application/json"
        }

        response = requests.post(url, json=payload, headers=headers)

        print(response.text)


        return Response({'status': 1,'responseData':{'message':json.loads(response.text)}},status=200)
