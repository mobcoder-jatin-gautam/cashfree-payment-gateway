import logo from './logo.svg';
import './App.css';
import React, { useEffect, useState } from 'react';
import ApiInstance from './interceptor';
import axios from 'axios';


function App() {

  const [values,setValues] = useState({})

  const makepayment = () => {

      ApiInstance.post('v1/user/make-payment/').then((res) => {
        // console.log(res.data.responseData.message);
        setValues(res.data.responseData)
        console.log(values.message,'----------');
    })      
    }

   const payOrder = () => {
    ApiInstance.post('v1/user/order-pay/').then((res) => {
      console.log(res.data.responseData.message.data.payload.web,'----------');
      window.location.replace(res.data.responseData.message.data.payload.web)
  })
   } 

  return (
      
		<div className="App">

    <button onClick={makepayment}>Create Payment</button>
    <button onClick={payOrder}>Pay Order</button>


  </div>
  );
}

export default App;
