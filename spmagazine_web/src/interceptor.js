import axios from "axios";

const ApiInstance = axios.create({
    baseURL: "http://127.0.0.1:8000",
  });
  

ApiInstance.interceptors.request.use(
    (request) => {
      
      let token = localStorage.getItem("token");
      const headers = {
        "Content-Type": "application/json",
      };

    //   headers["token"] = token;
  
    //   request.headers = headers;
      return request;
    },
    (error) => Promise.reject(error)
  );


ApiInstance.interceptors.response.use(
    (response) => {

        // console.log(response.data.statusCode,'1111111111')
  
      return response;
    },(error) => {
        console.log(error.response,'00000')

        if(error.response.data.status=='0'){
            console.log(1111)
            localStorage.removeItem("token")
            window.location.reload();
            window.location.href('/login')
        }
    })

export default ApiInstance;