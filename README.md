# Cashfree Payment Gateway



## Django project setup

- create a virtual env in spMagazine_api folder
- install all the packages for django project using
```
pip install -r requirement.txt
```
- migrate DB
```
python3 manage.py makemigratios
python3 manage.py migrate
```
- run Django server
```
python3 manage.py runserver
```

## React project setup

- Install all the packages spMagazine_web folder
```
npm i
```
- start react server
```
npm start
```

## Project Info

refer this url https://docs.cashfree.com/reference/createorder

Cashfree has its 4 api's

- create order  		// generate a order
- preauthorization of order    // get info about order status
- pay order  			// pay order using user details
- order details  		//get details of created order

we used create order api in our django project. It will generate a session_id and order_id .
and other 3 api's we will hit from frontend

## Another Method

- open cpg.py and change order ID
- when ever you run the script it will generate  apyment link by which you can pay through multiple ways
- refer this link https://payments-test.cashfree.com/order/#x2nomYTUCspjxzLzFbcx
